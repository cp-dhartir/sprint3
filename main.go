package main

import (
	"database/sql"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func init() {
	db, _ = sql.Open("mysql", "root:root@/practical3")
}

func main() {

	r := gin.Default()

	r.Static("/playlists", "./playlists")
	r.Static("/tracks", "./tracks")
	r.Static("/albums", "./albums")
	r.Static("/uploads", "./uploads")

	store := cookie.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	r.POST("/user/login", userLogin)

	r.POST("/user/register", userRegister)

	r.POST("/admin/login", adminLogin)

	r.POST("/admin/register", adminRegister)

	r.GET("/logout", authMiddleware(), logout)

	albumtrackrouter := r.Group("/album-tracks", authMiddleware())

	albumtrackrouter.GET("", callGetAlbumTracks)

	albumtrackrouter.GET("/:id", callGetAlbumTrack)

	userrouter := r.Group("/favourites", authMiddleware())

	userrouter.GET("", callGetFavouriteTracks)
	userrouter.POST("", callAddFavouriteTrack)

	router := r.Group("/playlist", adminAuthMiddleware())

	router.GET("", callGetPlaylists)
	router.POST("", callAddPlaylist)
	router.GET("/:id", callGetPlaylist)
	router.PUT("/:id", callUpdatePlaylist)
	router.PUT("/tracks/:id", callUpdatePlaylistTracks)

	router.DELETE("/:id", callDeletePlaylist)
	router.GET("", callGetTracks)
	router.POST("", callAddTrack)
	router.GET("/:id", callGetTrack)
	router.PUT("/:id", callUpdateTrack)
	router.DELETE("/:id", callDeleteTrack)

	router.GET("", callGetAlbums)
	router.POST("", callAddAlbum)
	router.GET("/:id", callGetAlbum)
	router.PUT("/:id", callUpdateAlbum)
	router.DELETE("/:id", callDeleteAlbum)

	router.POST("", callAddArtist)
	router.PUT("/:id", callUpdateArtist)
	router.DELETE("/:id", callDeleteArtist)

	r.Run(":8000")

	defer db.Close()
}
