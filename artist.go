package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func callGetArtists(c *gin.Context) {
	artists, err := getArtists()
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "List of artists",
			"dataset": artists,
		})
	}
}

func getArtists() ([]Artist, error) {
	db, err := connect()

	if err != nil {
		return nil, err
	}

	var artists []Artist

	rows, err := db.Query("SELECT id, name, img_url FROM artists")

	if err != nil {
		return nil, err
	}
	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var artist Artist
		err := rows.Scan(&artist.Id, &artist.Name, &artist.Image_url)
		if err != nil {
			return nil, err
		}
		artists = append(artists, artist)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return artists, nil
}

func callAddArtist(c *gin.Context) {
	_, err := addArtist(c)

	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		appResponse(c, http.StatusOK, "success", "Artist added successfully")
	}
}

func addArtist(c *gin.Context) (int64, error) {

	name := c.PostForm("name")
	file, _, err := c.Request.FormFile("image")

	if err != nil {
		return 0, err
	}

	defer file.Close()

	tempFile, err := ioutil.TempFile("uploads", "upload-*.png")

	if err != nil {
		return 0, err
	}

	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return 0, err
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)

	db, err := connect()

	if err != nil {
		return 0, err
	}

	img_url := "http://" + c.Request.Host + "/" + tempFile.Name()

	result, err := db.Exec("INSERT INTO artists (name, img_url) VALUES (?, ?)", name, img_url)

	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()

	if err != nil {
		return 0, err
	}
	return id, nil
}

func callGetArtist(c *gin.Context) {
	id := c.Param("id")
	artist, err := getArtist(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "Data of Artist",
			"data":    artist,
		})
	}
}

func getArtist(id string) (Artist, error) {

	var artist Artist

	db, err := connect()

	if err != nil {
		return artist, err
	}

	row := db.QueryRow("SELECT id, name, img_url FROM artists WHERE id = ?", id)

	err1 := row.Scan(&artist.Id, &artist.Name, &artist.Image_url)

	if err1 != nil {
		if err1 == sql.ErrNoRows {
			log.Fatal("no such artist")
		}
		return artist, err1
	}
	return artist, nil
}

func callUpdateArtist(c *gin.Context) {
	id := c.Param("id")
	_, err := updateArtist(c, id)

	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, http.StatusOK, "success", "Artist updated successfully")
	}
}

func updateArtist(c *gin.Context, id string) (int64, error) {

	db, err := connect()

	if err != nil {
		return 0, err
	}

	name := c.PostForm("name")
	file, _, err := c.Request.FormFile("image")

	if err != nil {
		return 0, err
	}

	defer file.Close()

	var img_url string

	if file == nil {

		var artist Artist

		row := db.QueryRow("SELECT id, img_url FROM artists WHERE id = ?", id)

		err := row.Scan(&artist.Id, &artist.Image_url)
		if err != nil {
			return 0, err
		}

		img_url = artist.Image_url

	} else {
		tempFile, err := ioutil.TempFile("uploads", "upload-*.png")

		if err != nil {
			return 0, err
		}

		defer tempFile.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			return 0, err
		}
		// write this byte array to our temporary file
		tempFile.Write(fileBytes)

		img_url = "http://" + c.Request.Host + "/" + tempFile.Name()

	}

	result, err := db.Exec("UPDATE artists SET name = ?, img_url = ?, updated_at = ? WHERE id = ?", name, img_url, time.Now().UTC(), id)
	if err != nil {
		return 0, err
	}

	artistId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return artistId, nil

}

func callDeleteArtist(c *gin.Context) {
	id := c.Param("id")
	err := deleteArtist(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		appResponse(c, http.StatusOK, "success", "Artist deleted successfully")
	}
}

func deleteArtist(id string) error {

	db, err := connect()

	if err != nil {
		return err
	}

	if _, err := db.Exec("DELETE FROM artists WHERE id = ?", id); err != nil {
		return err
	}
	return nil
}
