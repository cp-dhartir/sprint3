package main

import (
	"database/sql"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func callGetFavouriteTracks(c *gin.Context) {
	session := sessions.Default(c)
	user_id := session.Get("id")
	tracks, err := getFavouriteTracks(user_id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "List of tracks",
			"dataset": tracks,
		})
	}
}

func getFavouriteTracks(user_id interface{}) ([]Track, error) {
	db, err := connect()

	if err != nil {
		return nil, err
	}

	var tracks []Track

	rows, err := db.Query("SELECT id, user_id, track_id, favourite_index FROM favourite_tracks WHERE user_id = ?", user_id)

	if err != nil {
		return nil, err
	}

	for rows.Next() {

		var favourite Favourite_Track

		err := rows.Scan(&favourite.Id, &favourite.User_id, &favourite.Track_id, &favourite.Favourite_index)
		if err != nil {
			return nil, err
		}

		rows, err := db.Query("SELECT id, album_id, name, track_index, track_url, img_url, is_published FROM tracks WHERE id = ?", favourite.Track_id)

		if err != nil {
			return nil, err
		}
		// Loop through rows, using Scan to assign column data to struct fields.
		for rows.Next() {
			var track Track
			err := rows.Scan(&track.Id, &track.Album_id, &track.Name, &track.Track_index, &track.Track_url, &track.Image_url, &track.Is_published)
			if err != nil {
				return nil, err
			}
			tracks = append(tracks, track)
		}

		if err := rows.Err(); err != nil {
			return nil, err
		}
	}

	return tracks, nil
}

func callAddFavouriteTrack(c *gin.Context) {
	session := sessions.Default(c)
	user_id := session.Get("id")
	track_id := c.PostForm("track_id")
	ans, err := addFavouriteTrack(user_id, track_id)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		if ans == "added" {
			appResponse(c, http.StatusOK, "success", "Track added successfully")

		} else if ans == "removed" {
			appResponse(c, http.StatusOK, "success", "Track removed successfully")
		}
	}
}

func addFavouriteTrack(user_id interface{}, track_id string) (string, error) {

	db, err := connect()

	if err != nil {
		return "", err
	}

	var favourite Favourite_Track

	row := db.QueryRow("SELECT id, user_id, track_id, favourite_index FROM favourite_tracks WHERE user_id = ? AND track_id = ?", user_id, track_id)

	err1 := row.Scan(&favourite.Id, &favourite.User_id, &favourite.Track_id, &favourite.Favourite_index)

	if err1 != nil {
		if err1 == sql.ErrNoRows {
			result, err := db.Exec("INSERT INTO favourite_tracks (user_id, track_id, favourite_index) VALUES (?, ?, ?)",
				user_id, track_id, 1)

			if err != nil {
				return "", err
			}
			if _, err := result.LastInsertId(); err != nil {
				return "", err
			}
			return "added", nil
		}
		return "", err1
	}

	if _, err := db.Exec("DELETE FROM favourite_tracks WHERE id = ?", favourite.Id); err != nil {
		return "", err
	}
	return "removed", nil

}
