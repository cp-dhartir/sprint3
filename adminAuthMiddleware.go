package main

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func adminAuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {
		session := sessions.Default(c)
		role := session.Get("role")
		token := session.Get("token")
		apitoken := c.Request.Header.Get("token")

		msg := ValidateToken(apitoken)

		if token == apitoken && msg == nil {
			if role == nil {
				appResponse(c, 401, "unauthorized", "Please login first")
				c.Abort()

			} else if role == "user" {
				appResponse(c, 401, "unauthorized", "You can not access this page")
				c.Abort()

			} else if role == "admin" {
				c.Next()

			} else {
				appResponse(c, 401, "unauthorized", "Please login first")
				c.Abort()
			}
		} else {
			appResponse(c, 401, "unauthorized", "Token mismatch")
			c.Abort()
		}
	}
}
