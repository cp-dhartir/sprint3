package main

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func adminLogin(c *gin.Context) {
	userId, authtoken, err := login(User{
		email:    c.PostForm("email"),
		password: c.PostForm("password"),
		role:     "admin",
	})

	if err != nil {
		appResponse(c, http.StatusUnauthorized, "error", "This credentials do not match our records")
		return
	}
	session := sessions.Default(c)
	role := session.Get("role")
	if role == nil {
		session.Set("id", userId)
		session.Set("role", "admin")
		session.Set("token", authtoken)
	} else {
		session.Clear()
		session.Set("id", userId)
		session.Set("role", "admin")
		session.Set("token", authtoken)
	}
	session.Save()
	c.Header("token", authtoken)
	c.JSON(200, gin.H{
		"status":  "success",
		"message": "Admin login successfully",
	})
}

func userLogin(c *gin.Context) {
	userId, authtoken, err := login(User{
		email:    c.PostForm("email"),
		password: c.PostForm("password"),
		role:     "user",
	})
	if err != nil {
		appResponse(c, http.StatusUnauthorized, "error", "This credentials do not match our records")
		return
	}
	session := sessions.Default(c)
	role := session.Get("role")
	if role == nil {
		session.Set("id", userId)
		session.Set("role", "user")
		session.Set("token", authtoken)
	} else {
		session.Clear()
		session.Set("id", userId)
		session.Set("role", "user")
		session.Set("token", authtoken)
	}
	session.Save()
	c.Header("token", authtoken)
	c.JSON(200, gin.H{
		"status":  "success",
		"message": "User login successfully",
	})

}

func login(usr User) (int64, string, error) {

	userPassword := usr.password

	var user User

	row := db.QueryRow("SELECT id, email, password FROM users WHERE email = ? AND role = ?", usr.email, usr.role)

	if err := row.Scan(&user.Id, &user.email, &user.password); err != nil {
		return 0, "", err
	}

	userdbpwd := []byte(user.password)

	if err := bcrypt.CompareHashAndPassword(userdbpwd, []byte(userPassword)); err != nil {
		return 0, "", err
	}

	tokenString, err := GenerateJWT(user.email)

	// c.Request.Header.Set("token", tokenString)

	if err != nil {
		return 0, "", err
	}

	return user.Id, tokenString, nil
}

func adminRegister(c *gin.Context) {
	id, admintoken, err := register(User{
		name:     c.PostForm("name"),
		email:    c.PostForm("email"),
		password: c.PostForm("password"),
		role:     "admin",
	}, c)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Please try again")
	} else {
		session := sessions.Default(c)
		role := session.Get("role")
		if role == nil {
			session.Set("id", id)
			session.Set("role", "admin")
			session.Set("token", admintoken)
		} else {
			session.Clear()
			session.Set("id", id)
			session.Set("role", "admin")
			session.Set("token", admintoken)
		}
		session.Save()
		c.Header("token", admintoken)
		appResponse(c, http.StatusOK, "success", "Admin register successfully")
	}
}

func userRegister(c *gin.Context) {
	id, usertoken, err := register(User{
		name:     c.PostForm("name"),
		email:    c.PostForm("email"),
		password: c.PostForm("password"),
		role:     "user",
	}, c)

	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Please try again")
	} else {
		session := sessions.Default(c)
		role := session.Get("role")
		if role == nil {
			session.Set("id", id)
			session.Set("role", "user")
			session.Set("token", usertoken)
		} else {
			session.Clear()
			session.Set("id", id)
			session.Set("role", "user")
			session.Set("token", usertoken)
		}
		session.Save()
		c.Header("token", usertoken)
		appResponse(c, http.StatusOK, "success", "User register successfully")
	}
}

func register(usr User, c *gin.Context) (int64, string, error) {

	userPassword := usr.password

	// Generate "hash" to store from user password
	hash, err := bcrypt.GenerateFromPassword([]byte(userPassword), bcrypt.DefaultCost)
	if err != nil {
		return 0, "", err
	}

	result, err := db.Exec("INSERT INTO users (name, email, password, role) VALUES (?, ?, ?, ?)", usr.name, usr.email, string(hash), usr.role)

	if err != nil {
		return 0, "", err
	}

	if _, err := result.LastInsertId(); err != nil {
		return 0, "", err
	}

	id, authtoken, _ := login(User{
		email:    usr.email,
		password: usr.password,
		role:     usr.role,
	})

	return id, authtoken, nil
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	session.Clear()
	session.Save()
	appResponse(c, http.StatusOK, "success", "Logout successfully")
}
