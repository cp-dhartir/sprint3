package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func callGetAlbums(c *gin.Context) {
	albums, err := getAlbums()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "List of albums",
			"dataset": albums,
		})
	}
}

func getAlbums() ([]Album, error) {

	var albums []Album

	rows, err := db.Query("SELECT id, artist_id, name, description, img_url, published_at, is_published FROM albums")

	if err != nil {
		return nil, err
	}
	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var album Album
		var tracks []Track

		err := rows.Scan(&album.Id, &album.Artist_id, &album.Name, &album.Description, &album.Image_url, &album.Published_at, &album.Is_published)
		if err != nil {
			return nil, err
		}

		rows, err := db.Query("SELECT id, name, track_index, track_url, img_url, is_published FROM tracks WHERE album_id = ?", album.Id)

		if err != nil {
			return nil, err
		}

		for rows.Next() {
			var Track Track
			err := rows.Scan(&Track.Id, &Track.Name, &Track.Track_index, &Track.Track_url, &Track.Image_url, &Track.Is_published)

			if err != nil {
				return nil, err
			}

			tracks = append(tracks, Track)
		}
		album.Tracks = tracks

		albums = append(albums, album)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return albums, nil
}

func callAddAlbum(c *gin.Context) {
	_, err := addAlbum(c)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, http.StatusOK, "success", "Album added successfully")
	}
}

func addAlbum(c *gin.Context) (int64, error) {

	artist_id := c.PostForm("artist_id")
	name := c.PostForm("name")
	description := c.PostForm("description")
	published_at := time.Now().UTC()
	is_published := c.PostForm("is_published")
	file, _, err := c.Request.FormFile("image")

	if err != nil {
		return 0, err
	}

	defer file.Close()

	tempFile, err := ioutil.TempFile("albums", "album-*.png")

	if err != nil {
		return 0, err
	}

	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return 0, err
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)

	img_url := "http://127.0.0.1:8000/" + tempFile.Name()

	result, err := db.Exec("INSERT INTO albums (artist_id, name, description, img_url, published_at, is_published) VALUES (?, ?, ?, ?, ?, ?)",
		artist_id, name, description, img_url, published_at, is_published)

	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()

	if err != nil {
		return 0, err
	}
	return id, nil
}

func callGetAlbum(c *gin.Context) {
	id := c.Param("id")
	album, err := getAlbum(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "Data of Album",
			"data":    album,
		})
	}
}

func getAlbum(id string) (Album, error) {

	var album Album

	row := db.QueryRow("SELECT id, artist_id, name, description, img_url, published_at, is_published FROM albums WHERE id = ?", id)

	err1 := row.Scan(&album.Id, &album.Artist_id, &album.Name, &album.Description, &album.Image_url, &album.Published_at, &album.Is_published)

	if err1 != nil {
		if err1 == sql.ErrNoRows {
			log.Fatal("no such album")
		}
		return album, err1
	}

	var tracks []Track

	rows, err := db.Query("SELECT id, name, track_index, track_url, img_url, is_published FROM tracks WHERE album_id = ?", album.Id)

	if err != nil {
		return album, err
	}

	for rows.Next() {
		var Track Track
		err := rows.Scan(&Track.Id, &Track.Name, &Track.Track_index, &Track.Track_url, &Track.Image_url, &Track.Is_published)

		if err != nil {
			return album, err
		}

		tracks = append(tracks, Track)
	}
	album.Tracks = tracks

	return album, nil
}

func callUpdateAlbum(c *gin.Context) {
	id := c.Param("id")
	_, err := updateAlbum(c, id)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, http.StatusOK, "success", "Album updated successfully")
	}
}

func updateAlbum(c *gin.Context, id string) (int64, error) {

	var album Album
	var img_url string

	artist_id := c.PostForm("artist_id")
	name := c.PostForm("name")
	description := c.PostForm("description")
	published_at := c.PostForm("published_at")
	is_published := c.PostForm("is_published")
	file, _, err := c.Request.FormFile("image")

	if file == nil {
		row := db.QueryRow("SELECT id, img_url FROM albums WHERE id = ?", id)
		err := row.Scan(&album.Id, &album.Image_url)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Fatal("no such album")
			}
			return 0, err
		}

		img_url = album.Image_url

	} else {

		if err != nil {
			return 0, err
		}

		defer file.Close()

		tempFile, err := ioutil.TempFile("albums", "album-*.png")

		if err != nil {
			return 0, err
		}

		defer tempFile.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			return 0, err
		}
		// write this byte array to our temporary file
		tempFile.Write(fileBytes)

		img_url = "http://127.0.0.1:8000/" + tempFile.Name()

	}

	result, err := db.Exec("UPDATE albums SET artist_id = ?, name = ?, description = ?, img_url = ?, published_at = ?, is_published = ?, updated_at = ? WHERE id = ?",
		artist_id, name, description, img_url, published_at, is_published, time.Now().UTC(), id)
	if err != nil {
		return 0, err
	}

	albumId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return albumId, nil
}

func callDeleteAlbum(c *gin.Context) {
	id := c.Param("id")
	err := deleteAlbum(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		appResponse(c, http.StatusOK, "success", "Album deleted successfully")
	}
}

func deleteAlbum(id string) error {

	if _, err := db.Exec("DELETE FROM albums WHERE id = ?", id); err != nil {
		return err
	}
	return nil
}
