package main

import (
	_ "github.com/go-sql-driver/mysql"
)

type User struct {
	Id       int64
	name     string
	email    string
	password string
	role     string
}

type Artist struct {
	Id        int64
	Name      string
	Image_url string
}

type Album struct {
	Id           int64
	Artist_id    int64
	Name         string
	Description  string
	Image_url    string
	Published_at string
	Is_published bool
	Tracks       []Track
}

type Track struct {
	Id           int64
	Album_id     int64
	Name         string
	Track_index  int64
	Track_url    string
	Image_url    string
	Is_published bool
}

type Playlist struct {
	Id           int64
	Name         string
	Description  string
	Image_url    string
	Is_published bool
	Tracks       []Track
}

type Favourite_Track struct {
	Id              int64
	User_id         int64
	Track_id        int64
	Favourite_index int64
}
