package main

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func callGetAlbumTracks(c *gin.Context) {
	albums, err := getAlbumTracksList()
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "List of albums",
			"dataset": albums,
		})
	}
}

func getAlbumTracksList() ([]Album, error) {
	db, err := connect()

	if err != nil {
		return nil, err
	}

	var albums []Album

	rows, err := db.Query("SELECT id, name, description, img_url, published_at, is_published FROM albums")

	if err != nil {
		return nil, err
	}
	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var Album Album
		var tracks []Track

		err := rows.Scan(&Album.Id, &Album.Name, &Album.Description, &Album.Image_url, &Album.Published_at, &Album.Is_published)
		if err != nil {
			return nil, err
		}

		rows, err := db.Query("SELECT id, name, track_index, track_url, img_url, is_published FROM tracks WHERE album_id = ?", Album.Id)

		if err != nil {
			return nil, err
		}

		for rows.Next() {
			var Track Track
			err := rows.Scan(&Track.Id, &Track.Name, &Track.Track_index, &Track.Track_url, &Track.Image_url, &Track.Is_published)

			if err != nil {
				return nil, err
			}

			tracks = append(tracks, Track)
		}
		Album.Tracks = tracks

		albums = append(albums, Album)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return albums, nil
}

func callGetAlbumTrack(c *gin.Context) {
	id := c.Param("id")
	album, err := getAlbumTrack(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "Data of album",
			"data":    album,
		})
	}
}

func getAlbumTrack(id string) (Album, error) {

	var Album Album

	db, err := connect()

	if err != nil {
		return Album, err
	}

	row := db.QueryRow("SELECT id, name, description, img_url, published_at, is_published FROM albums WHERE id = ?", id)

	err1 := row.Scan(&Album.Id, &Album.Name, &Album.Description, &Album.Image_url, &Album.Published_at, &Album.Is_published)

	if err1 != nil {
		if err1 == sql.ErrNoRows {
			log.Fatal("no such album")
		}
		return Album, err1
	}

	var tracks []Track

	rows, err := db.Query("SELECT id, name, track_index, track_url, img_url, is_published FROM tracks WHERE album_id = ?", Album.Id)

	if err != nil {
		return Album, err
	}

	for rows.Next() {
		var Track Track
		err := rows.Scan(&Track.Id, &Track.Name, &Track.Track_index, &Track.Track_url, &Track.Image_url, &Track.Is_published)

		if err != nil {
			return Album, err
		}

		tracks = append(tracks, Track)
	}
	Album.Tracks = tracks

	return Album, nil
}
