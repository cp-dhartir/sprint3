package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func callGetPlaylists(c *gin.Context) {
	playlists, err := getPlaylists()
	if err != nil {
		appResponse(c, 404, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "List of playlists",
			"dataset": playlists,
		})
	}
}

func getPlaylists() ([]Playlist, error) {

	var playlists []Playlist

	rows, err := db.Query("SELECT id, name, description, img_url, is_published FROM playlists")

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var playlist Playlist
		var tracks []Track

		err := rows.Scan(&playlist.Id, &playlist.Name, &playlist.Description, &playlist.Image_url, &playlist.Is_published)
		if err != nil {
			return nil, err
		}

		rows, err := db.Query("SELECT track_id FROM playlist_tracks WHERE playlist_id = ?", playlist.Id)

		if err != nil {
			return nil, err
		}

		for rows.Next() {
			var t_id int64
			var track Track

			err := rows.Scan(&t_id)

			if err != nil {
				return nil, err
			}

			row := db.QueryRow("SELECT id, album_id, name, track_index, track_url, img_url, is_published FROM tracks WHERE id = ?", t_id)

			err1 := row.Scan(&track.Id, &track.Album_id, &track.Name, &track.Track_index, &track.Track_url, &track.Image_url, &track.Is_published)

			if err1 != nil {
				return nil, err1
			}

			tracks = append(tracks, track)
		}

		playlist.Tracks = tracks

		playlists = append(playlists, playlist)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return playlists, nil
}

func callAddPlaylist(c *gin.Context) {
	_, err := addPlaylist(c)

	if err != nil {
		c.JSON(404, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, 200, "success", "Playlist added successfully")
	}
}

func addPlaylist(c *gin.Context) (int64, error) {

	name := c.PostForm("name")
	description := c.PostForm("description")
	is_published := c.PostForm("is_published")
	file, _, err := c.Request.FormFile("image")

	if err != nil {
		return 0, err
	}

	defer file.Close()

	tempFile, err := ioutil.TempFile("playlists", "playlist-*.png")

	if err != nil {
		return 0, err
	}

	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return 0, err
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)

	img_url := "http://127.0.0.1:8000/" + tempFile.Name()

	result, err := db.Exec("INSERT INTO playlists (name, description, img_url, is_published) VALUES (?, ?, ?, ?)",
		name, description, img_url, is_published)

	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()

	if err != nil {
		return 0, err
	}

	return id, nil
}

func callGetPlaylist(c *gin.Context) {
	id := c.Param("id")
	playlist, err := getPlaylist(id)
	if err != nil {
		appResponse(c, 404, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "Data of Playlist",
			"data":    playlist,
		})
	}
}

func getPlaylist(id string) (Playlist, error) {

	var playlist Playlist
	var tracks []Track

	row := db.QueryRow("SELECT id, name, description, img_url, is_published FROM playlists WHERE id = ?", id)

	err1 := row.Scan(&playlist.Id, &playlist.Name, &playlist.Description, &playlist.Image_url, &playlist.Is_published)

	if err1 != nil {
		if err1 == sql.ErrNoRows {
			log.Fatal("no such playlist")
		}
		return playlist, err1
	}

	rows, err := db.Query("SELECT track_id FROM playlist_tracks WHERE playlist_id = ?", playlist.Id)

	if err != nil {
		return playlist, err
	}

	for rows.Next() {
		var t_id int64
		var track Track

		err := rows.Scan(&t_id)

		if err != nil {
			return playlist, err
		}

		row := db.QueryRow("SELECT id, album_id, name, track_index, track_url, img_url, is_published FROM tracks WHERE id = ?", t_id)

		err1 := row.Scan(&track.Id, &track.Album_id, &track.Name, &track.Track_index, &track.Track_url, &track.Image_url, &track.Is_published)

		if err1 != nil {
			return playlist, err1
		}

		tracks = append(tracks, track)
	}
	playlist.Tracks = tracks

	return playlist, nil
}

func callUpdatePlaylist(c *gin.Context) {
	id := c.Param("id")
	_, err := updatePlaylist(c, id)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, 200, "success", "Playlist updated successfully")
	}
}

func updatePlaylist(c *gin.Context, id string) (int64, error) {

	var playlist Playlist
	var img_url string

	name := c.PostForm("name")
	description := c.PostForm("description")
	is_published := c.PostForm("is_published")
	file, _, err := c.Request.FormFile("image")

	if file == nil {
		row := db.QueryRow("SELECT id, img_url FROM playlists WHERE id = ?", id)
		err := row.Scan(&playlist.Id, &playlist.Image_url)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Fatal("no such playlist")
			}
			return 0, err
		}

		img_url = playlist.Image_url

	} else {

		if err != nil {
			return 0, err
		}

		defer file.Close()

		tempFile, err := ioutil.TempFile("playlists", "playlist-*.png")

		if err != nil {
			return 0, err
		}

		defer tempFile.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			return 0, err
		}
		// write this byte array to our temporary file
		tempFile.Write(fileBytes)

		img_url = "http://127.0.0.1:8000/" + tempFile.Name()

	}

	result, err := db.Exec("UPDATE playlists SET name = ?, description = ?, img_url = ?, is_published = ?, updated_at = ? WHERE id = ?",
		name, description, img_url, is_published, time.Now().UTC(), id)
	if err != nil {
		return 0, err
	}

	playlistId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return playlistId, nil
}

func callUpdatePlaylistTracks(c *gin.Context) {

	err := updatePlaylistTracks(c)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, 200, "success", "Playlist tracks updated successfully")
	}
}

func updatePlaylistTracks(c *gin.Context) error {

	tracks := c.PostFormArray("tracks_id")
	id := c.Param("id")

	rows, err := db.Query("SELECT track_id FROM playlist_tracks WHERE playlist_id = ?", id)

	if err != nil {
		return err
	}

	for rows.Next() {
		var t_id int64
		err := rows.Scan(&t_id)
		if err != nil {
			return err
		}
		if _, err := db.Exec("DELETE FROM playlist_tracks WHERE track_id = ?", t_id); err != nil {
			return err
		}
	}

	for _, value := range tracks {
		ids := strings.Split(value, ",")

		for _, v := range ids {
			_, err := db.Exec("INSERT INTO playlist_tracks (playlist_id, track_id) VALUES (?, ?)",
				id, v)

			if err != nil {
				return err
			}
		}
	}

	return nil
}

func callDeletePlaylist(c *gin.Context) {
	id := c.Param("id")
	err := deletePlaylist(id)
	if err != nil {
		appResponse(c, 404, "error", "Something went wrong")
	} else {
		appResponse(c, 200, "success", "Playlist deleted successfully")
	}
}

func deletePlaylist(id string) error {

	if _, err := db.Exec("DELETE FROM playlists WHERE id = ?", id); err != nil {
		return err
	}
	return nil
}
