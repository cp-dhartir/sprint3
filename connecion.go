package main

import "database/sql"

func connect() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:root@/practical3")

	if err != nil {
		return db, err
	}

	return db, nil
}
