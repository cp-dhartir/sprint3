package main

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func middleware() gin.HandlerFunc {

	return func(c *gin.Context) {
		session := sessions.Default(c)
		token := session.Get("token")
		apitoken := c.Request.Header.Get("token")

		msg := ValidateToken(apitoken)

		if token == apitoken && msg == nil {
			c.Next()
		} else {
			appResponse(c, http.StatusUnauthorized, "unauthorized", "Access denied")
			c.Abort()
		}
	}
}
