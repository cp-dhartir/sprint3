package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func callGetTracks(c *gin.Context) {
	tracks, err := getTracks()
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "List of tracks",
			"dataset": tracks,
		})
	}
}

func getTracks() ([]Track, error) {
	db, err := connect()

	if err != nil {
		return nil, err
	}

	var tracks []Track

	rows, err := db.Query("SELECT id, album_id, name, track_index, track_url, img_url, is_published FROM tracks")

	if err != nil {
		return nil, err
	}
	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var track Track
		err := rows.Scan(&track.Id, &track.Album_id, &track.Name, &track.Track_index, &track.Track_url, &track.Image_url, &track.Is_published)
		if err != nil {
			return nil, err
		}
		tracks = append(tracks, track)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}
	return tracks, nil
}

func callAddTrack(c *gin.Context) {
	_, err := addTrack(c)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, http.StatusOK, "success", "Track added successfully")
	}
}

func addTrack(c *gin.Context) (int64, error) {

	album_id := c.PostForm("album_id")
	name := c.PostForm("name")
	track_index := c.PostForm("track_index")
	track_url := c.PostForm("track_url")
	is_published := c.PostForm("is_published")
	file, _, err := c.Request.FormFile("image")

	if err != nil {
		return 0, err
	}

	defer file.Close()

	tempFile, err := ioutil.TempFile("tracks", "track-*.png")

	if err != nil {
		return 0, err
	}

	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return 0, err
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)

	db, err := connect()

	if err != nil {
		return 0, err
	}

	img_url := "http://127.0.0.1:8000/" + tempFile.Name()

	result, err := db.Exec("INSERT INTO tracks (album_id, name, track_index, track_url, img_url, is_published) VALUES (?, ?, ?, ?, ?, ?)",
		album_id, name, track_index, track_url, img_url, is_published)

	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()

	if err != nil {
		return 0, err
	}
	return id, nil
}

func callGetTrack(c *gin.Context) {
	id := c.Param("id")
	track, err := getTrack(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		c.JSON(http.StatusOK, gin.H{
			"message": "Data of Track",
			"data":    track,
		})
	}
}

func getTrack(id string) (Track, error) {

	var track Track

	db, err := connect()

	if err != nil {
		return track, err
	}

	row := db.QueryRow("SELECT id, album_id, name, track_index, track_url, img_url, is_published FROM tracks WHERE id = ?", id)

	err1 := row.Scan(&track.Id, &track.Album_id, &track.Name, &track.Track_index, &track.Track_url, &track.Image_url, &track.Is_published)

	if err1 != nil {
		if err1 == sql.ErrNoRows {
			log.Fatal("no such track")
		}
		return track, err1
	}
	return track, nil
}

func callUpdateTrack(c *gin.Context) {
	id := c.Param("id")
	_, err := updateTrack(c, id)

	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status":  "error",
			"message": err,
		})
	} else {
		appResponse(c, http.StatusOK, "success", "Track updated successfully")
	}
}

func updateTrack(c *gin.Context, id string) (int64, error) {

	db, err := connect()

	if err != nil {
		return 0, err
	}

	var track Track
	var img_url string

	album_id := c.PostForm("album_id")
	name := c.PostForm("name")
	track_index := c.PostForm("track_index")
	track_url := c.PostForm("track_url")
	is_published := c.PostForm("is_published")
	file, _, err := c.Request.FormFile("image")

	if file == nil {
		row := db.QueryRow("SELECT id, img_url FROM tracks WHERE id = ?", id)
		err := row.Scan(&track.Id, &track.Image_url)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Fatal("no such track")
			}
			return 0, err
		}

		img_url = track.Image_url

	} else {

		if err != nil {
			return 0, err
		}

		defer file.Close()

		tempFile, err := ioutil.TempFile("tracks", "track-*.png")

		if err != nil {
			return 0, err
		}

		defer tempFile.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			return 0, err
		}
		// write this byte array to our temporary file
		tempFile.Write(fileBytes)

		img_url = "http://127.0.0.1:8000/" + tempFile.Name()

	}

	result, err := db.Exec("UPDATE tracks SET album_id = ?, name = ?, track_index = ?, track_url = ?, img_url = ?, is_published = ?, updated_at = ? WHERE id = ?",
		album_id, name, track_index, track_url, img_url, is_published, time.Now().UTC(), id)
	if err != nil {
		return 0, err
	}

	trackId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return trackId, nil
}

func callDeleteTrack(c *gin.Context) {
	id := c.Param("id")
	err := deleteTrack(id)
	if err != nil {
		appResponse(c, http.StatusNotFound, "error", "Something went wrong")
	} else {
		appResponse(c, http.StatusOK, "success", "Track deleted successfully")
	}
}

func deleteTrack(id string) error {

	db, err := connect()

	if err != nil {
		return err
	}

	if _, err := db.Exec("DELETE FROM tracks WHERE id = ?", id); err != nil {
		return err
	}
	return nil
}
