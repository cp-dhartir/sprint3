package main

import (
	"github.com/gin-gonic/gin"
)

func appResponse(c *gin.Context, code int, status string, message string) {
	c.JSON(code, gin.H{
		"status":  status,
		"message": message,
	})
}
